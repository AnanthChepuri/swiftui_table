//
//  Teams.swift
//  SwiftUI_Table
//
//  Created by Ananth Chepuri on 07/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation
import SwiftUI

struct Team {
    let country: String
    let teamName: String
    let teamColor: Color
}
