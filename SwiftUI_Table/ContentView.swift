//
//  ContentView.swift
//  SwiftUI_Table
//
//  Created by Ananth Chepuri on 07/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct Teams: Identifiable {
    var id: Int
    
    let country: String
    let teamName: String
    let teamColor: Color
}

struct ContentView: View {
    var teamList = [
        Teams(id: 1, country: "India", teamName: "Team India", teamColor: .blue),
        Teams(id: 2, country: "Australia", teamName: "Aussies", teamColor: .init(red: 255/255, green: 255/255, blue: 0/255)),
        Teams(id: 3, country: "New Zealand", teamName: "Black caps", teamColor: .black),
        Teams(id: 4, country: "South Africa", teamName: "Proteas", teamColor: .green),
        Teams(id: 5, country: "England", teamName: "English men", teamColor: .init(red: 0/255, green: 0/255, blue: 153/255)),
        Teams(id: 6, country: "Sri Lanks", teamName: "Lankans", teamColor: .init(red: 51/255, green: 51/255, blue: 255/255)),
        Teams(id: 7, country: "West Indies", teamName: "Caribbeans", teamColor: .init(red: 102/255, green: 75/255, blue: 0/255))
        
    ]
    
    var body: some View {
        NavigationView {
            List(teamList, id: \.country) { team in
                HStack {
                    Text(team.country)
                        .bold()
                        .font(.headline)
                    Text(team.teamName)
                        .font(.subheadline)
                        .foregroundColor(team.teamColor)
                }
            }
            .navigationBarTitle("Teams")
            .navigationBarItems(trailing: Button(action: addTeam, label: { Text("+")}))
        }
    }
    
    func addTeam() {
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
